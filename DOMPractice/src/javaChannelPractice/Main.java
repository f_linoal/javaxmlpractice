package javaChannelPractice;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Main {
    private static final int BUFFER_SIZE = 25;
    private static final String inPathStr = "./inputData/longText.txt";
    private static final String outPathStr = "./outputData/longText_out.txt";

    public static void main(String[] args){
        copy(inPathStr, outPathStr);
    }

    private static void copy(String inFilePath, String outFilePath){
        System.out.println("copy start");
        System.out.println("-------------");
        try(  // try-with resources. closeし忘れを防げる。
              // Channelはnewできない。IOストリームからgetChannelして得る。
                FileChannel srcChannel = new FileInputStream(inFilePath).getChannel();
                FileChannel destChannel = new FileOutputStream(outFilePath).getChannel()
                ){

            ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE); // allocateDirectoryを使うとJVMのヒープ外に配置。

            while(true){
                buffer.clear();
                debugPrint(buffer);

                if(srcChannel.read(buffer) < 0){ break; } // 最後まで読むと戻り値は-1に。
                // ここでbufferのpositionは、readした分だけ0から移動。
                // つまり [0, position]の範囲に出力すべきデータがある。
                buffer.flip();// limit←position; position←0
                destChannel.write(buffer); // [position, limit]の範囲（入力した分を全て）を出力する。
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        System.out.println("-------------");
        System.out.println("copy end");
    }

    private static void debugPrint(ByteBuffer buf){
        try{
            ByteBuffer buf2 = buf.duplicate();
            byte[] bytes = new byte[buf.limit()];
            buf2.get(bytes);
            System.out.println(new String(bytes, "UTF-8"));
        }catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
    }
}


// 参考文献：http://itpro.nikkeibp.co.jp/article/COLUMN/20060424/236102/