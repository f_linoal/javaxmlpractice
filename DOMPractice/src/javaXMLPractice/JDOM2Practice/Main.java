package javaXMLPractice.JDOM2Practice;

import org.jdom2.Comment;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.FileWriter;
import java.io.IOException;

public class Main {
    private static final String inPathStr = "./inputData/xmlExample_DOM.xml";
    private static final String outPathStr = "./outputData/xmlExample_JDOM2_out.xml";

    public static void main(String[] args){
        // ファイルから読む
        Document fileDoc = readXMLFile(inPathStr);
        println("**** read from xml file ****\n");
        printDocument(fileDoc);

        // データアクセス・削除
        manipulateSomeContents(fileDoc);
        println("\n\n\n**** deleted something ****\n");
        printDocument(fileDoc);

        // コードで1からxmlを構築する
        Document genDoc = generateDOMDocument();
        println("\n\n\n**** code-generated xml ****\n");
        printDocument(genDoc);
        saveXML(genDoc);
        println("genDoc saved.");
    }

    private static Document readXMLFile(String path){
        Document doc = null;
        SAXBuilder builder = new SAXBuilder();
        try{
            doc = builder.build(path);
        }catch(IOException | JDOMException e){
            e.printStackTrace();
        }
        return doc;
    }

    private static Document generateDOMDocument(){
        // ドキュメントとルートノード生成
        Element carElement = new Element("car");
        Document document = new Document(carElement); // XMLの仕様上、Documentには必ず1つだけのルートエレメントがあると決まっているのでコンストラクタで渡す
        carElement.setAttribute("serialNumber", "ABC123");

        // <car>に<madeBy>Toyota</madeBy>を追加
        Element madeBy = new Element("madeBy");
        madeBy.addContent("Toyota");
        carElement.addContent(madeBy);
        // 上を1行で書くと carElement.addContent(new Element("make").addContent("Toyota"));

        carElement.addContent(new Comment("Description of car"));

        return document;
    }

    private static void printDocument(Document doc){
        XMLOutputter xmlOut = new XMLOutputter();
        xmlOut.setFormat(Format.getPrettyFormat()); // インデント、エンコーディングなど細かい設定もできる
        String xmlStr = xmlOut.outputString(doc);
        println(xmlStr);
    }

    private static void manipulateSomeContents(Document doc){
        Element rootElem = doc.getRootElement();

        // genderのコンテンツを削除
        Element genderElem = rootElem.getChild("gender");// 該当がない場合はnullになる
        println("deleting " + genderElem.getContent());
        genderElem.removeContent();

        Element nameElement = rootElem.getChild("name");
        if( nameElement == null ){ println("element not found"); }
        nameElement.getParent().removeContent(nameElement);
    }

    private static void saveXML(Document doc){
        XMLOutputter xmlOut = new XMLOutputter();
        xmlOut.setFormat(Format.getPrettyFormat());
        try{
            xmlOut.output(doc, new FileWriter(outPathStr));
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    private static void println(String msg){System.out.println(msg);}
}
