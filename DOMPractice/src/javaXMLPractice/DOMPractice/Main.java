package javaXMLPractice.DOMPractice;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    private static final String inPathStr = "./inputData/xmlExample_DOM.xml";
    private static final String outPathStr = "./outputData/xmlExample_DOM_out.xml";

    public static void main(String[] args){
        Node root = getRootFromPath(inPathStr); // xml解析
        println("\n\n\n**** tree of source xml ****\n");
        printTree(root, 0);
        addSomeNodesTo(root); // ノードを書き足す
        println("\n\n\n**** added ****\n");
        printTree(root, 0);
        deleteSomeNodesIn(root); // ノードを切り離す
        println("\n\n\n**** deleted ****\n");
        printTree(root, 0);
        saveXML((Document)root); // セーブ
        println("\n\nsaved.");

    }

    private static Node getRootFromPath(String pathStr){
        Node root = null;
        try{
            DocumentBuilderFactory docBF = DocumentBuilderFactory.newInstance();
            //docBF.setValidating(true);
            DocumentBuilder builder = docBF.newDocumentBuilder();
            Path inPath = Paths.get(pathStr).normalize().toRealPath();
            println("loading " + inPath);
            root = builder.parse(inPath.toString());

            // DOCTYPE読み込み
            DocumentType dtd = ((Document)root).getDoctype();
            if( dtd != null ){
                println("DOCTYPE root name : " + dtd.getName());
                print("DOCTYPE node info : "); printNodeInfo(dtd, 0); // DOCTYPEもノードのうち
            }

        }catch(ParserConfigurationException | java.io.IOException  | SAXException e){
            e.printStackTrace();
        }
        return root;
    }

    private static void printNodeInfo(Node node, int tabDepth){
        printTab(tabDepth);

        // print node info
        String nodeName = node.getNodeName();
        String nodeValue = node.getNodeValue();
        String nodeAttrs = attributesToStr(node);

        if( nodeValue==null ) nodeValue="_"; // タグのノードにおいては value は null. タグ間の文字列が #text ノードとなり value を持つ。
        else nodeValue = nodeValue.replace("\n", ""); // Valueには改行だけの文字列が多いため、見栄えのため取り除く。
        System.out.printf("<%s%s> '%s'\n", nodeName, nodeAttrs, nodeValue);
    }

    // 再帰で木を描画
    private static void printTree(Node node, int tabDepth){
        if( tabDepth < 0 || tabDepth > 99999 ){ println("invalid arg, or nested too deep"); return; }
        printNodeInfo(node, tabDepth);
        NodeList children = node.getChildNodes(); // この方法は遅いので、 getFirstChild()から getNextSibling() でイテレートする方法をとろう。
        for(int i=0; i<children.getLength(); i++){
            Node child = children.item(i);
            printTree(child, tabDepth+1);
        }
    }

    private static String attributesToStr(Node node){
        NamedNodeMap attrs = node.getAttributes();
        if( attrs==null ) return "";
        StringBuilder strB = new StringBuilder("");
        for( int i=0; i<attrs.getLength(); i++){
            Node attr = attrs.item(i); // attributeもノード
            String attrName = attr.getNodeName();
            String attrValue = attr.getNodeValue();
           if( attr.getAttributes() != null) println("Something went wrong"); // Attributeの入れ子が無いことの確認
            strB.append(String.format(" %s=%s", attrName, attrValue));
        }
        return strB.toString();
    }

    private static void addSomeNodesTo(Node root){
        Document doc = (Document)root;
        Node newElem = doc.createElement("age");
        Node newElemValue = doc.createTextNode("25");
        newElem.appendChild(newElemValue);  // 要素ノード"age"の子の末尾にテキストノード"25"を付加

        Node profileNode = searchChildTagByName("profile", root);

        // jobタグを探し当てる
        //Node jobNode = profileNode.getChildNodes().item(5); // ←現実的でない方法。無意味な空白textノードが紛れ込む状況で、正確なインデックスを事前に知るのは難しい。
        Node jobNode = searchChildTagByName("job", profileNode); // ← 検索プログラムを組むほうが現実的。

        profileNode.insertBefore(newElem, jobNode); // ageノードをprofileNodeの直下に追加。場所はjobNodeの直前を指定。

        Element profileElement = (Element)profileNode; // 属性ノードを追加
        profileElement.setAttribute("lastUpdate", "20140801");

    }

    private static void deleteSomeNodesIn(Node root){
        Node profileNode = searchChildTagByName("profile", root);
        Node nameNode = searchChildTagByName("name", profileNode);
        //root.removeChild(nameNode); // これはエラー。引数は、自身より1代だけ下のノードに限る。
        nameNode.getParentNode().removeChild(nameNode);
    }

    private static void saveXML(Document document){
        try{
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new FileWriter(new File(outPathStr)));
            transformer.setOutputProperty(OutputKeys.INDENT,  "yes"); // 設定
            transformer.transform(source, result);
        }catch( IOException | TransformerException e){
            e.printStackTrace();
        }
    }


    private static Node searchChildTagByName(String searchTagName, Node parentNode){
        Node child = parentNode.getFirstChild();
        while((child = child.getNextSibling()) != null){
            if( child.getNodeType() == Node.ELEMENT_NODE && child.getNodeName().equals(searchTagName) ){
                return child;
            }
        }
        return null;
    }

    private static void println(String msg){ System.out.println(msg); }
    private static void print(String msg){ System.out.print(msg); }
    private static void printTab(int tabDepth){
        if(tabDepth<0){ println("invalid arg"); return; }
        for(int i=0; i<tabDepth; i++){
            print("|\t");
        }
    }

}

class ParseErrorHandler extends DefaultHandler {

}
